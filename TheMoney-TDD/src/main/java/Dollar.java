/**
 * Created by fredericradigoy on 18/09/2016.
 */
public class Dollar {

    public int amount;

    Dollar(int amount){

        this.amount = amount;

    }

    void times(int multiplier){

        amount = amount * multiplier;

    }

}
